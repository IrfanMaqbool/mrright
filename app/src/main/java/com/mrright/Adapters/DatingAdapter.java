package com.mrright.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.mrright.Model.StartDatingModel.Match;
import com.mrright.fragment.DatingFragment;
import com.mrright.Model.SignInModel.Data;

import java.util.ArrayList;

/**
 * Created by deadgame on 07/06/2018.
 */
// dating screen ,setting one user at a time.
public class DatingAdapter extends FragmentPagerAdapter {

    private ArrayList<Match> matches;
// init user one at a time
    public DatingAdapter(FragmentManager fm, ArrayList<Match> matches) {
        super(fm);
        this.matches = matches;
    }
// get user one at a time
    @Override
    public Fragment getItem(int position) {
        DatingFragment datingFragment = new DatingFragment();
        datingFragment.setData(matches.get(position));
        return datingFragment;
    }
// set total user count
    @Override
    public int getCount() {
        return matches.size();

    }

}
