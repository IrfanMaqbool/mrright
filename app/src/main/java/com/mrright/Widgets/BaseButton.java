package com.mrright.Widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.widget.Button;

import com.mrright.R;

/**
 * Created by deadgame on 07/06/2018.
 */
// base button class  for app
public class BaseButton extends Button {
    public BaseButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs,
                R.styleable.APP_ATTRIBUTES);
        try {
            String customFont = a.getString(R.styleable.APP_ATTRIBUTES_fontName);
            setCustomFont(customFont);
        } finally {
            a.recycle();
        }
        initializeProperties();
    }

    private void initializeProperties() {
        setAllCaps(false);
    }

    public boolean setCustomFont(String asset) {

        Typeface tf = null;
        try {
            tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + asset);
        } catch (Exception e) {
            return false;
        }

        setTypeface(tf);
        return true;
    }
}
