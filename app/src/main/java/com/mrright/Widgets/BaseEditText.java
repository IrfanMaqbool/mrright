package com.mrright.Widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.mrright.R;
import com.mrright.Util.Methods;

/**
 * Created by deadgame on 07/06/2018.
 */
// base EditText class for app
public class BaseEditText extends EditText {

    private String bgCheck;
// init class
    public BaseEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }
// set custom font
    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs,
                R.styleable.APP_ATTRIBUTES);
        try {
            String customFont = a.getString(R.styleable.APP_ATTRIBUTES_fontName);
            bgCheck = a.getString(R.styleable.APP_ATTRIBUTES_isBg);
            setCustomFont(customFont);
        } finally {
            a.recycle();
        }

        initializeProperties();


    }
// init and check bg properties
    private void initializeProperties() {
        if ((!Methods.isNullOrEmptyString(bgCheck)) && (bgCheck.equals("0"))) {
            setBackgroundColor(Color.TRANSPARENT);
        } else {
            setBackgroundResource(R.drawable.edit_text_bg);
        }
        setGravity(Gravity.CENTER_VERTICAL);
        setHintTextColor(getResources().getColor(R.color.edit_text_hint_color));
        setTextColor(Color.BLACK);
        setSingleLine(true);
    }
// set custom fonts
    public boolean setCustomFont(String asset) {

        Typeface tf = null;
        try {
            tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + asset);
        } catch (Exception e) {
            return false;
        }

        setTypeface(tf);
        return true;
    }
// check validations
    public boolean validate() {
        if (!Methods.isNullOrEmptyString(getText().toString())) {
            return true;
        }
        requestFocus();
        setError(getResources().getString(R.string.please_fill_this_field));
        return false;
    }
// view is single line or not
    public void setViewSingleLine(boolean isViewSingleLine) {
        setSingleLine(isViewSingleLine);
    }
}
