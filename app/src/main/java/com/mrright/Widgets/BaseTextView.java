package com.mrright.Widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.mrright.R;
import com.mrright.Util.Methods;

/**
 * Created by deadgame on 07/06/2018.
 */
// base TextView class
public class BaseTextView extends TextView {

    public BaseTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs,
                R.styleable.APP_ATTRIBUTES);
        try {
            String customFont = a.getString(R.styleable.APP_ATTRIBUTES_fontName);
            setCustomFont(customFont);
        } finally {
            a.recycle();
        }
//        initializeProperties()

    }

    public boolean setCustomFont(String asset) {

        Typeface tf = null;
        try {
            tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + asset);
        } catch (Exception e) {
            return false;
        }

        setTypeface(tf);
        return true;
    }

    public boolean validate() {
        if (!Methods.isNullOrEmptyString(getText().toString())) {
            return true;
        }
        requestFocus();
        setError(getResources().getString(R.string.please_fill_this_field));
        return false;
    }
}
