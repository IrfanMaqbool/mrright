package com.mrright.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mrright.Model.SignInModel.Data;
import com.mrright.Model.StartDatingModel.Match;
import com.mrright.R;
import com.mrright.Widgets.BaseTextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by deadgame on 07/06/2018.
 */
// show one user on screen for dating according to preferences
public class DatingFragment extends Fragment {
    private BaseTextView age, fullName, matchPercentage, gender, friendshipStatus, aboutMe, personalInterest;
    private Match match;
// init view
    public DatingFragment() {
    }

// init screen
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.item_dating, container, false);

        initViews(rootView);
        setView();

        return rootView;
    }
// set user values
    private void setView() {

        String year = "";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'");
        try {
            Date date = format.parse(match.getDob());
            Date currentDate = new Date();
            year = String.valueOf(currentDate.getYear() - date.getYear());


            System.out.println(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        age.setText(year);
        fullName.setText(match.getFull_name());
        matchPercentage.setText(String.valueOf(match.getYes_res_count()) + "%");
        gender.setText(match.getGender());
        friendshipStatus.setText(Html.fromHtml(friendshipStatus.getText().toString() + " " + "<font color=\"#963E69\">" + match.getPreferences().getLooking_for() + "</font>"));
        aboutMe.setText(match.getAboutMe());
        String interest = "";

        if (null != match.getPersonal_interests() && match.getPersonal_interests().size() > 0) {
            for (String personalInterest : match.getPersonal_interests()) {
                String preValue = interest.equals("") ? interest : interest.toString() + " , ";
                interest = preValue + personalInterest;
            }
        }

        personalInterest.setText(interest);


    }
// init view
    private void initViews(ViewGroup rootView) {
        age = rootView.findViewById(R.id.age);
        fullName = rootView.findViewById(R.id.fullName);
        matchPercentage = rootView.findViewById(R.id.matchPercentage);
        gender = rootView.findViewById(R.id.gender);
        friendshipStatus = rootView.findViewById(R.id.friendshipStatus);
        aboutMe = rootView.findViewById(R.id.aboutMe);
        personalInterest = rootView.findViewById(R.id.personalInterest);
    }
    
// getting user data
    public void setData(Match match) {
        this.match = match;
    }
}
