package com.mrright.Util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;

import com.mrright.R;
import com.mrright.Widgets.BaseTextView;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by deadgame on 07/06/2018.
 */
// common methods
public class Methods {

    public static String convertToJsonObj(HashMap<String, String> params) {

        return "";
    }
// string format user in app
    public static String stringFormat(Calendar calendar, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
        return sdf.format(calendar.getTime());
    }
// show alert with ok button in app
    public static void showSingleButtonDialog(Activity activity, String msg) {
        new AlertDialog.Builder(activity)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }
    // show alert with ok and cancel button
    public static void showDoubleButtonDialog(Activity activity, String msg,DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity).setMessage(msg).setCancelable(false);
        builder.setPositiveButton("OK", listener);
        builder.setNegativeButton("Cancel", listener);
        builder.show();
    }
// dialog picker with 3 options
    public static void dialogPicker(Dialog dialog, View.OnClickListener listener) {
        dialogPickerWithString(null, null, null, null, dialog, listener);
    }
// dialog picker with 3 options
    public static void dialogPickerWithString(String title, String text1, String text2, String text3, Dialog dialog, View.OnClickListener listener) {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialog);

        BaseTextView dialogTitle = dialog.findViewById(R.id.dialogTitle);
        BaseTextView option1 = dialog.findViewById(R.id.option1);
        BaseTextView option2 = dialog.findViewById(R.id.option2);
        BaseTextView option3 = dialog.findViewById(R.id.option3);

        if (!isNullOrEmptyString(title)) {
            dialogTitle.setText(title);
        }
        if (!isNullOrEmptyString(text1)) {
            option1.setText(text1);
        }
        if (!isNullOrEmptyString(text2)) {
            option2.setText(text2);
        }
        if (!isNullOrEmptyString(text3)) {
            option3.setText(text3);
        }

        option1.setOnClickListener(listener);
        option2.setOnClickListener(listener);
        option3.setOnClickListener(listener);
        dialog.show();
    }
// checking whether string is empty or not
    public static boolean isNullOrEmptyString(String text) {
        if (null == text || text.equalsIgnoreCase("null") || text.isEmpty()) {
            return true;
        }
        return false;
    }

}

