package com.mrright.Util;

/**
 * Created by deadgame on 06/06/2018.
 */
// hardcode variables values used in app
public class Constants {
    public static final String FB_PUBLIC_PROFILE = "public_profile";
    public static final String FB_EMAIL = "email";
    public static final String FB_Name = "full_name";
    public static final String FB_BIRTHDAY = "user_birthday";
    public static final String FB_ID = "FbId";
    public static final String APP_PREFERENCES = "MR_RIGHT";
    public static final String DOB_FORMAT = "MM/dd/yyyy";
    public static final String LOGIN_STATUS = "loginStatus";
    public static String VALUE_YES = "1";
    public static String VALUE_NO = "0";
// api params used in app
    public static class ApiParams {
        public static final String FB_ID = "id";
        public static final String FB_NAME = "name";
        public static final String FB_EMAIL = "email";
        public static final String FB_ID_Login = "fbid";
        public static final String FULL_NAME = "Full_name";
        public static final String EMAIL = "email";
        public static final String GENDER = "gender";
        public static final String LAT = "lat";
        public static final String LONG = "long";
        public static final String PHONE = "phone";
        public static final String ABOUT_ME = "aboutMe";
        public static final String DOB = "dob";
        /*Pref Params;*/
        public static final String PREF_AGE_MIN = "age_min";
        public static final String PREF_AGE_MAX = "age_max";
        public static final String PREF_DISTANCE = "dist";
        public static final String PREF_RELIGION = "religion";
        public static final String PREF_GENDER = "gender";
        public static final String PREF_LOOKING_FOR = "looking_for";
        public static final String PREF_PERSONAL_INTEREST = "pi";


    }

    public static final String NO_USER_EXIT_STRING = "No such user exists";
    public static final int STATUS_Failure = 404;
    public static final int STATUS_SUCCESS = 200;

    public static final String RELIGION_INTENT = "religionIntent";
    public static final int RELIGION_CODE = 1;
    public static final int PERSONAL_INTEREST_CODE = 2;
    public static final int REQUEST_CODE = 1;
    public static final String PREF_KEY = "PREF_KEY";


    public static final String[] RELIGIONS_ARRAY = {"None", "Buddism", "Christianity", "Catholicism",
            "Hinduism", "Islam", "Jewish", "Sikh", "Other"};
    public static final String[] PERSONAL_INTEREST_ARRAY = {"Concerts", "Volunteer Events", "Beach", "Live Theater", "Comedy Clubs",
            "Museums", "Antiquing", "Card Games/Bridge Canasta", "Painting/Drawing", "Video Games/Online Games",
            "Board Games/Backgammon/Chess", "Cooking/Barbecuing", "Photography", "Wine Testing", "Camping", "Gardening", "Poetry", "Travelling/Weekend Trips/Adventure Travel", "Basketball", "Golf", "Snow Skiing/Snowboarding",
            "Biking", "Hiking/Walking", "Tennis/Racquet Sports", "Dancing", "Jogging/Running", "Yoga/Meditation"};

    public static final String RELIGION = "None | Buddism | Christianity | Catholicism | Hinduism | Islam | Jewish | Sikh | Other";
    public static final String PERSONAL_INTEREST = "Concerts | Volunteer Events | Beach | Live Theater | Comedy Clubs | Museums | Antiquing | Card Games/Bridge Canasta | Painting/Drawing | Video Games/Online Games | Board Games/Backgammon/Chess | Cooking/Barbecuing | Photography | Wine Testing | Camping | Gardening | Poetry | Travelling/Weekend Trips/Adventure Travel | Basketball | Golf | Snow Skiing/Snowboarding | Biking | Hiking/Walking | Tennis/Racquet Sports | Dancing | Jogging/Running | Yoga/Meditation";

    public static final String LookingFor1 = "Friendship";
    public static final String LookingFor2 = "Relationship";
    public static final String LookingFor3 = "Other";
}
