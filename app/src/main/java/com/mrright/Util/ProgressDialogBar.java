package com.mrright.Util;

import android.app.Activity;
import android.app.ProgressDialog;

/**
 * Created by deadgame on 09/06/2018.
 */
// common progress bar user in app.
public class ProgressDialogBar {
    ProgressDialog progressdialog;
    private static ProgressDialogBar instance;
    private ProgressDialog dialog;

    private ProgressDialogBar() {

    }
// init bar
    public static ProgressDialogBar getInstance() {
        if (instance == null) {
            instance = new ProgressDialogBar();
        }
        return instance;
    }
// show progress bar
    public void showProgressBar(Activity activity) {
        progressdialog = new ProgressDialog(activity);
        progressdialog.setMessage("Processing...");
        progressdialog.setCancelable(false);
        progressdialog.show();
    }
// cancel progress bar
    public void disMissDialog() {
        if (progressdialog != null && progressdialog.isShowing())
            progressdialog.dismiss();
    }


}
