package com.mrright.Util;

import android.content.Context;
import android.content.SharedPreferences;

import com.mrright.R;

/**
 * Created by deadgame on 06/06/2018.
 */
// app local data save in this class, which required even user close app
public class AppPreferences {

    private static AppPreferences instance;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor appEditor;


    private AppPreferences() {
    }

    public static AppPreferences getInstance() {
        if (instance == null) {
            instance = new AppPreferences();
        }
        return instance;
    }

// init local data saving class
    public void initAppPreferences(Context context) {
        sharedPref = context.getSharedPreferences(
                Constants.APP_PREFERENCES, Context.MODE_PRIVATE);
        appEditor = sharedPref.edit();
    }
// get user email
    public String getEmail() {
        return sharedPref.getString(Constants.FB_EMAIL, "-1");
    }
// save user email
    public void setEmail(String email) {
        appEditor.putString(Constants.FB_EMAIL, email);
        appEditor.commit();
    }
// get user fb id
    public String getFbId() {
        return sharedPref.getString(Constants.FB_ID, "-1");
    }
// save user fb id
    public void setFbId(String FbId) {
        appEditor.putString(Constants.FB_ID, FbId);
        appEditor.commit();
    }
// get user full name of fb
    public String getFbFullName() {
        return sharedPref.getString(Constants.FB_Name, "-1");
    }
    // save user full name of fb
    public void setFbFullName(String fullName) {
        appEditor.putString(Constants.FB_Name, fullName);
        appEditor.commit();
    }

// get login status
    public String getLoginStatus() {
        return sharedPref.getString(Constants.LOGIN_STATUS, Constants.VALUE_NO);
    }
// save login status
    public void setLoginStatus(String loginStatus) {
        appEditor.putString(Constants.LOGIN_STATUS, loginStatus);
        appEditor.commit();
    }
// remove all data if user sign out .
    public void signOut() {
        appEditor.clear().commit();
    }

// get religion preferences of users
    public String getReligion() {
        return sharedPref.getString(Constants.ApiParams.PREF_RELIGION, Cache.religion);
    }
    // set religion preferences of users
    public void setReligion(String religion) {
        appEditor.putString(Constants.ApiParams.PREF_RELIGION, religion);
        appEditor.commit();
    }
}
