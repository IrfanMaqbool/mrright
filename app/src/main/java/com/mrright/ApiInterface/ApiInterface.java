package com.mrright.ApiInterface;

import com.mrright.Model.SignInModel.SignInModel;
import com.mrright.Model.StartDatingModel.StartDatingModel;

import org.json.JSONObject;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;


/**
 * Created by deadgame on 06/06/2018.
 */
// api's using in app
public interface ApiInterface {
    @Headers({"Content-Type: application/json"})
    @POST("login")
    Call<SignInModel> login(@Body String body);

    @Headers({"Content-Type: application/json"})
    @POST("register")
    Call<SignInModel> register(@Body String body);

    @Headers({"Content-Type: application/json"})
    @POST("getprofiles/{id}")
    Call<StartDatingModel> getProfiles(@Path("id") String userId, @Body String body);

    @Headers({"Content-Type: application/json"})
    @POST("update_prefs/{id}")
    Call<SignInModel> updatePrefs(@Path("id") String userId, @Body String body);

    @Headers({"Content-Type: application/json"})
    @POST("update_pi/{id}")
    Call<SignInModel> updatePersonalInterest(@Path("id") String userId, @Body String body);
}
