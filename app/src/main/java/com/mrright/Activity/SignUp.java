package com.mrright.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.Login;
import com.mrright.ApiInterface.ApiInterface;
import com.mrright.Model.SignInModel.SignInModel;
import com.mrright.R;
import com.mrright.Util.ApiClient;
import com.mrright.Util.AppPreferences;
import com.mrright.Util.Cache;
import com.mrright.Util.Constants;
import com.mrright.Util.Methods;
import com.mrright.Util.ProgressDialogBar;
import com.mrright.Widgets.BaseEditText;
import com.mrright.Widgets.BaseTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by deadgame on 07/06/2018.
 */
// register user class
public class SignUp extends BaseActivity implements View.OnClickListener {

    private BaseEditText fullName, email, dob, gender, phone, aboutMe;

    Button signUp;
    Calendar calendar = Calendar.getInstance();
    ;
    private DatePickerDialog dobDialog;
    private LocationManager lm;
    private boolean isLocationGet;
// init activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        setView();
    }
// set prePopulated values form fb
    private void setView() {
        fullName.setText(AppPreferences.getInstance().getFbFullName());
        email.setText(AppPreferences.getInstance().getEmail());

        requestPermission();
    }
// inti view
    private void initView() {
        fullName = findViewById(R.id.fullName);
        email = findViewById(R.id.email);
        dob = findViewById(R.id.dob);
        gender = findViewById(R.id.gender);
        phone = findViewById(R.id.phone);
        aboutMe = findViewById(R.id.aboutMe);
        signUp = findViewById(R.id.signUp);

        initDobPciker();


        dob.setOnClickListener(this);
        gender.setOnClickListener(this);
        signUp.setOnClickListener(this);
    }
// init dob picker
    private void initDobPciker() {
        dobDialog = new DatePickerDialog(this, dobListerner, calendar
                .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
        calendar.add(Calendar.YEAR, -18);
        dobDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
    }

// dob listener
    DatePickerDialog.OnDateSetListener dobListerner = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateDob();
        }

    };
// set user selected dob value
    private void updateDob() {
        dob.setText(Methods.stringFormat(calendar, Constants.DOB_FORMAT));
    }

// handle click listener of fields
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.dob:
                dobDialog.show();
                break;
            case R.id.gender:
                showGenderDialog();
                break;
            case R.id.signUp:
                if (validate()) {
                    checkReqPermission();
                }
                break;
        }
    }
// check validations
    private boolean validate() {
        if ((!fullName.validate()) || (!email.validate()) || (!dob.validate()) || (!gender.validate()) || (!phone.validate()) || (!aboutMe.validate())) {
            return false;
        }
        return true;
    }
// show gender dialog
    private void showGenderDialog() {
        showDialog();
    }

// call common gender dialog
    public void showDialog() {
        final Dialog genderDialog = new Dialog(this);
        Methods.dialogPicker(genderDialog, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gender.setText(((BaseTextView) view).getText().toString());
                genderDialog.dismiss();
            }
        });

    }
// check location permissions
    private void checkReqPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermission();
            return;
        } else {
            if (!isGpsOn()) {
                askToOnGps();
                return;
            }
        }

        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (null == location) {
            ProgressDialogBar.getInstance().showProgressBar(this);
            isLocationGet = false;
            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 100, 1, listener);
            return;
        }
        registerUser(location);
    }
 // sign up user.
    private void registerUser(Location location) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        JSONObject object = new JSONObject();
        try {
            object.put(Constants.ApiParams.FB_ID_Login, AppPreferences.getInstance().getFbId());
            object.put(Constants.ApiParams.FULL_NAME, fullName.getText().toString());
            object.put(Constants.ApiParams.EMAIL, email.getText().toString());
            object.put(Constants.ApiParams.GENDER, gender.getText().toString());
            object.put(Constants.ApiParams.LAT, String.valueOf(location.getLatitude()));
            object.put(Constants.ApiParams.LONG, String.valueOf(location.getLongitude()));
            object.put(Constants.ApiParams.PHONE, phone.getText().toString());
            object.put(Constants.ApiParams.ABOUT_ME, aboutMe.getText().toString());
            object.put(Constants.ApiParams.DOB, dob.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Call<SignInModel> call = apiService.register(object.toString());
        call.enqueue(new Callback<SignInModel>() {
            @Override
            public void onResponse(Call<SignInModel> call, Response<SignInModel> response) {
                ProgressDialogBar.getInstance().disMissDialog();
                SignInModel signInModel = response.body();

                if (null != signInModel) {
                    Cache.userData = signInModel;
                    AppPreferences.getInstance().setLoginStatus(Constants.VALUE_YES);
                    successFullyLogin();
                }
            }

            @Override
            public void onFailure(Call<SignInModel> call, Throwable t) {
                ProgressDialogBar.getInstance().disMissDialog();
            }
        });
    }
// user registered successfully , call login screen.
    private void successFullyLogin() {
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        this.finish();
    }
// location listeners to get user location
    LocationListener listener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            if (!isLocationGet) {
                isLocationGet = true;
                registerUser(location);
            }
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };
// request user for his permission to get his location
    private void requestPermission() {
        if (!(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            if (!isGpsOn()) {
                askToOnGps();
            }
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    Constants.REQUEST_CODE);
        }
    }
    // user permission to get his location result
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (!isGpsOn()) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        }

    }
// open location permission screen
    private void askToOnGps() {
        Methods.showDoubleButtonDialog(this, "Please on gps", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int whichButton) {
                if (whichButton == -1) { //ok button
                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                }
                dialogInterface.dismiss();
            }
        });
    }
// checking whether user turn on location or not
    private boolean isGpsOn() {
        lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            return false;
        }
        return true;
    }

    // set layout , overRide from base class
    @Override
    public int getLayoutRes() {
        return R.layout.activity_sing_up;
    }

}
