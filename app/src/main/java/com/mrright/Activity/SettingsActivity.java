package com.mrright.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.renderscript.Float2;
import android.text.style.BackgroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarFinalValueListener;
import com.crystal.crystalrangeseekbar.interfaces.OnSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.crystal.crystalrangeseekbar.widgets.CrystalSeekbar;
import com.mrright.ApiInterface.ApiInterface;
import com.mrright.Model.SignInModel.Data;
import com.mrright.Model.SignInModel.SignInModel;
import com.mrright.R;
import com.mrright.Util.ApiClient;
import com.mrright.Util.AppPreferences;
import com.mrright.Util.Cache;
import com.mrright.Util.Constants;
import com.mrright.Util.Methods;
import com.mrright.Util.ProgressDialogBar;
import com.mrright.Widgets.BaseEditText;
import com.mrright.Widgets.BaseTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by deadgame on 07/06/2018.
 */
// settings screen
public class SettingsActivity extends BaseActivity {

    private String maxAge = "40", minAge = "18", distance = "100";
    private BaseEditText ageRangeBar, distanceValue, religion, personalInterest, gender, lookingFor;
    private CrystalRangeSeekbar ageRangeSeekbar;
    private CrystalSeekbar distanceSeekbar;
    private ArrayList<Integer> posList;

// init activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        fillData();
        setView();
    }
// set view
    private void setView() {
        setAgeSeekBar();
        setDistanceSeekBar();
    }
// set previous selected values
    private void fillData() {
        Data userData = Cache.userData.getData();
        if (userData.getPreferences().getAge_max() != 0) {
            maxAge = String.valueOf(userData.getPreferences().getAge_max());
            minAge = String.valueOf(userData.getPreferences().getAge_min());
            distance = String.valueOf(userData.getPreferences().getDist());
            gender.setText(userData.getPreferences().getGender().get(0));
            religion.setText(AppPreferences.getInstance().getReligion());
            setPrefInitValue(personalInterest, userData.getPersonal_interests());
            lookingFor.setText(userData.getPreferences().getLooking_for());
        }
    }
// init view
    private void initView() {
        ageRangeBar = findViewById(R.id.ageRangeBar);
        gender = findViewById(R.id.gender);
        distanceValue = findViewById(R.id.distanceValue);
        religion = findViewById(R.id.religion);
        personalInterest = findViewById(R.id.personalInterest);
        personalInterest.setViewSingleLine(false);
        lookingFor = findViewById(R.id.lookingFor);
        ageRangeSeekbar = findViewById(R.id.ageRangeSeekBar);
        distanceSeekbar = findViewById(R.id.distanceSeekbar);
    }
// set age seek bar
    private void setAgeSeekBar() {
        ageRangeSeekbar.setMinStartValue(Float.valueOf(minAge)).setMaxStartValue(Float.valueOf(maxAge)).apply();

        ageRangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                minAge = (String.valueOf(minValue));
                maxAge = (String.valueOf(maxValue));
                setAgeRangeValue();
            }
        });
    }
// set distance seekbar
    private void setDistanceSeekBar() {

        distanceSeekbar.setMinStartValue(Float.valueOf(distance)).apply();
        distanceSeekbar.setOnSeekbarChangeListener(new OnSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue) {
                distance = String.valueOf(minValue);
                setDistanceRangeValue();
            }
        });
    }
// set age range value
    private void setAgeRangeValue() {
        ageRangeBar.setText(minAge + "-" + maxAge);
    }
// set distance range value
    private void setDistanceRangeValue() {
        distanceValue.setText(distance + "km");
    }

// open religion preference screen
    public void religionBar(View view) {
        Intent religionIntent = getPreferenceIntent();
        religionIntent.putExtra("preValue", religion.getText().toString());
        religionIntent.putExtra(Constants.RELIGION_INTENT, Constants.RELIGION_INTENT);
        startActivityForResult(religionIntent, Constants.RELIGION_CODE);
    }
// open personalInterest preference screen
    public void personalInterest(View view) {
        Intent personalInterestIntent = getPreferenceIntent();
        personalInterestIntent.putExtra("preValue", personalInterest.getText().toString());
        startActivityForResult(personalInterestIntent, Constants.PERSONAL_INTEREST_CODE);
    }
// common intent of ChoosePreferenceActivity
    private Intent getPreferenceIntent() {
        return new Intent(this, ChoosePreferenceActivity.class);
    }
// handle ChoosePreferenceActivity results for religion && personalInterest preferences
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            posList = getResultHashMap(data);

            if (requestCode == Constants.RELIGION_CODE) {
                setPrefValue(religion, true);
            } else if (requestCode == Constants.PERSONAL_INTEREST_CODE) {
                setPrefValue(personalInterest, false);
            }
        }
    }
// get religion && personalInterest preferences result
    private ArrayList<Integer> getResultHashMap(Intent data) {
        Bundle wrapper = data.getBundleExtra(Constants.PREF_KEY);
        return (ArrayList<Integer>) wrapper.getSerializable(Constants.PREF_KEY);
    }
// set religion && personalInterest preferences value
    private void setPrefValue(BaseEditText view, boolean forView) {
        view.setText("");
        for (int index : posList) {
            String preValue = view.getText().toString().equals("") ? view.getText().toString() : view.getText().toString() + ", ";
            if (forView) {
                view.setText(preValue + Constants.RELIGIONS_ARRAY[index]);
            } else {
                view.setText(preValue + Constants.PERSONAL_INTEREST_ARRAY[index]);
            }
        }
    }
// set preference initial values
    private void setPrefInitValue(BaseEditText view, List<String> array) {
        view.setText("");
        for (String text : array) {
            String preValue = view.getText().toString().equals("") ? view.getText().toString() : view.getText().toString() + ", ";
            view.setText(preValue + text);
        }
    }
// save preferences by callings api's
    public void save(View view) {
        if (validate())
            updatePersonalInterest();
    }
// validations for preferences
    private boolean validate() {
        if ((!gender.validate()) || (!religion.validate()) || (!personalInterest.validate()) || (!lookingFor.validate())) {
            return false;
        }
        return true;
    }
// updatePersonalInterest api hit
    private void updatePersonalInterest() {

        ProgressDialogBar.getInstance().showProgressBar(this);


        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        JSONObject object = new JSONObject();
        try {
            object.put(Constants.ApiParams.PREF_PERSONAL_INTEREST, personalInterest.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Call<SignInModel> call = apiService.updatePersonalInterest(Cache.userData.getData().get_id(), object.toString());
        call.enqueue(new Callback<SignInModel>() {
            @Override
            public void onResponse(Call<SignInModel> call, Response<SignInModel> response) {
                SignInModel signInModel = response.body();
                if (null != signInModel) {
                    if (signInModel.getStatus() == Constants.STATUS_SUCCESS) {
                        updatePreferences();
                    }
                }

            }

            @Override
            public void onFailure(Call<SignInModel> call, Throwable t) {
                ProgressDialogBar.getInstance().disMissDialog();
            }
        });
    }
// update all preferences
    private void updatePreferences() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        JSONObject object = new JSONObject();
        try {
            object.put(Constants.ApiParams.PREF_AGE_MIN, minAge);
            object.put(Constants.ApiParams.PREF_AGE_MAX, maxAge);
            object.put(Constants.ApiParams.PREF_DISTANCE, distance);
            object.put(Constants.ApiParams.PREF_RELIGION, religion.getText().toString());
            object.put(Constants.ApiParams.PREF_GENDER, gender.getText().toString());
            object.put(Constants.ApiParams.PREF_LOOKING_FOR, lookingFor.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Call<SignInModel> call = apiService.updatePrefs(Cache.userData.getData().get_id(), object.toString());
        call.enqueue(new Callback<SignInModel>() {
            @Override
            public void onResponse(Call<SignInModel> call, Response<SignInModel> response) {
                ProgressDialogBar.getInstance().disMissDialog();
                SignInModel signInModel = response.body();
                if (null != signInModel) {
                    if (signInModel.getStatus() == Constants.STATUS_SUCCESS) {
                        Cache.userData = signInModel;
                        Methods.showSingleButtonDialog(SettingsActivity.this, signInModel.getMessage());
                        AppPreferences.getInstance().setReligion(religion.getText().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<SignInModel> call, Throwable t) {
                ProgressDialogBar.getInstance().disMissDialog();
            }
        });
    }

// show gender options
    public void showGenderDialog(View view) {
        showDialog();
    }
// show gender dialog box
    public void showDialog() {
        final Dialog genderDialog = new Dialog(this);
        Methods.dialogPicker(genderDialog, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gender.setText(((BaseTextView) view).getText().toString());
                genderDialog.dismiss();

            }
        });
    }
    // set this class header , overRide from base class
    @Override
    public String getHeaderTitle() {
        return getResources().getString(R.string.settings);
    }
    // set layout , overRide from base class
    @Override
    public int getLayoutRes() {
        return R.layout.activity_settings;
    }
// open looking for dialog
    public void lookingForBar(View view) {
        final Dialog lookingForDialog = new Dialog(this);

        Methods.dialogPickerWithString(getResources().getString(R.string.lookingFor), Constants.LookingFor1, Constants.LookingFor2, Constants.LookingFor3, lookingForDialog, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lookingFor.setText(((BaseTextView) view).getText().toString());
                lookingForDialog.dismiss();

            }
        });
    }
}
