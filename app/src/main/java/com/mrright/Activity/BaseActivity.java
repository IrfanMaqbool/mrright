package com.mrright.Activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.facebook.FacebookSdk;
import com.mrright.R;
import com.mrright.Widgets.BaseTextView;

import static java.security.AccessController.getContext;

/**
 * Created by deadgame on 05/06/2018.
 */
// base class of all activities
public abstract class BaseActivity extends FragmentActivity {
    protected AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.6F);
    BaseTextView header;
    BaseTextView leftTopBarIcon;
    RelativeLayout leftTopBar;
// set resource of all classes.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(getLayoutRes());
        initView();
    }
// init common header of all classes
    private void initView() {
        header = findViewById(R.id.headerTitle);
        leftTopBarIcon = findViewById(R.id.leftTopBarIcon);
        leftTopBar = findViewById(R.id.leftTopBar);
        if (null != header) {
            header.setText(getHeaderTitle());
        }
    }
// override this function in each activity
    public abstract int getLayoutRes();
// top back button click listener
    public void goBack(View view) {
        onBackPressed();
    }
// top header title
    public void setHeaderTitle(String title) {
        if (null != header) {
            header.setText(title);
        }
    }
    // set custom left button Img
    public void setLeftTopBarIcon(int resId) {
        if (null != leftTopBarIcon) {
            leftTopBarIcon.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(resId), null, null, null);
        }
    }
// set custom left button with img, message and bg
    public void setLeftTopBarIconWtihMsgAndBg(int resId, int msgId, int clrID) {
        if (null != leftTopBarIcon) {
            leftTopBarIcon.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(resId), null, null, null);
            leftTopBarIcon.setText(getResources().getString(msgId));
            leftTopBar.setBackgroundColor(getResources().getColor(clrID));
        }
    }
// override header in each activity if there is any header.
    public String getHeaderTitle() {
        return "";
    }
}


