package com.mrright.Activity;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;

import com.mrright.Adapters.DatingAdapter;
import com.mrright.ApiInterface.ApiInterface;
import com.mrright.Model.SignInModel.Data;
import com.mrright.Model.StartDatingModel.Match;
import com.mrright.Model.StartDatingModel.StartDatingModel;
import com.mrright.R;
import com.mrright.Util.ApiClient;
import com.mrright.Util.AppPreferences;
import com.mrright.Util.Cache;
import com.mrright.Util.ProgressDialogBar;
import com.mrright.Widgets.BaseTextView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by deadgame on 07/06/2018.
 */
// start dating screen
public class StartDatingActivity extends BaseActivity {

    private DatingAdapter pageAdapter;
    private ViewPager pager;

// init activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLeftTopBarIconWtihMsgAndBg(R.drawable.stop_dating, R.string.stopDating, R.color.appStyleColor);
        initViews();
        getProfileView(Cache.userData.getData().get_id());

    }
// get app users according to api's
    private void getProfileView(String userId) {

        ProgressDialogBar.getInstance().showProgressBar(this);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        JSONObject object = new JSONObject();


        Call<StartDatingModel> call = apiService.getProfiles(userId, object.toString());
        call.enqueue(new Callback<StartDatingModel>() {
            @Override
            public void onResponse(Call<StartDatingModel> call, Response<StartDatingModel> response) {
                ProgressDialogBar.getInstance().disMissDialog();
                StartDatingModel startDatingModel = response.body();
                if (null != startDatingModel && startDatingModel.getMatches().size() > 0) {
                    setAdapter(startDatingModel.getMatches());
                } else {
                    setNoMatchesFoundText();
                }

            }

            @Override
            public void onFailure(Call<StartDatingModel> call, Throwable t) {
                ProgressDialogBar.getInstance().disMissDialog();
                setNoMatchesFoundText();
            }
        });
    }
// set and show users one at a time
    private void setAdapter(ArrayList<Match> matches) {
        pageAdapter = new DatingAdapter(getSupportFragmentManager(), matches);
        pager.setAdapter(pageAdapter);
    }
// not user match according to preferences
    private void setNoMatchesFoundText() {
        findViewById(R.id.noMatchesFound).setVisibility(View.VISIBLE);
    }
// init view
    private void initViews() {
        pager = findViewById(R.id.viewpagerDating);


    }
    // set layout , overRide from base class
    @Override
    public int getLayoutRes() {
        return R.layout.activity_dating;
    }
}
