package com.mrright.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.mrright.ApiInterface.ApiInterface;
import com.mrright.Model.SignInModel.SignInModel;
import com.mrright.R;
import com.mrright.Util.ApiClient;
import com.mrright.Util.AppPreferences;
import com.mrright.Util.Cache;
import com.mrright.Util.Constants;
import com.mrright.Util.ProgressDialogBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
// fb login screen ,
public class MainActivity extends BaseActivity implements View.OnClickListener {

    ImageView fbBtn;
    private CallbackManager callbackManager;
// creadte view
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppPreferences.getInstance().initAppPreferences(this);
        checkLoginStatus();
    }
// check user registered status
    private void checkLoginStatus() {
        if (AppPreferences.getInstance().getLoginStatus().equals(Constants.VALUE_YES)) {
            login(AppPreferences.getInstance().getFbId());
        } else {
            initView();
        }
    }
// init view
    private void initView() {
        fbBtn = findViewById(R.id.fbBtn);
        fbBtn.setOnClickListener(this);
        registerFb();

    }
    // set layout , overRide from base class
    @Override
    public int getLayoutRes() {
        return R.layout.activity_main;
    }
// handle click of fb login
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fbBtn:
                view.startAnimation(buttonClick);
                openFb();
                break;
        }
    }
// open fb screen
    private void openFb() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList(Constants.FB_PUBLIC_PROFILE, Constants.FB_EMAIL));
    }
// init fb and handle callback after login
    private void registerFb() {

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        setFbGraphRequest(loginResult);
//                        AppPreferences.getInstance().setFbId(object.getString("id"));
                    }

                    @Override
                    public void onCancel() {
                        Log.d("a","cancel");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.d("a","error");
                    }
                });
    }
// get fb name and email
    private void setFbGraphRequest(LoginResult loginResult) {
        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),

                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            AppPreferences.getInstance().setFbId(object.getString(Constants.ApiParams.FB_ID));
                            AppPreferences.getInstance().setEmail(object.getString(Constants.ApiParams.EMAIL));
                            AppPreferences.getInstance().setFbFullName(object.getString(Constants.ApiParams.FB_NAME));
                            login(AppPreferences.getInstance().getFbId());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", Constants.ApiParams.FB_ID + "," + Constants.ApiParams.FB_NAME + "," + Constants.ApiParams.FB_EMAIL);
        request.setParameters(parameters);

        request.executeAsync();
    }
// handle fb result and sign up result
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_CODE) {
                dashBoard();
                return;
            }
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

// call sign in api after fb success
    private void login(final String fbId) {

        ProgressDialogBar.getInstance().showProgressBar(this);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        JSONObject object = new JSONObject();
        try {
            object.put(Constants.ApiParams.FB_ID_Login, fbId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Call<SignInModel> call = apiService.login(object.toString());
        call.enqueue(new Callback<SignInModel>() {
            @Override
            public void onResponse(Call<SignInModel> call, Response<SignInModel> response) {
                ProgressDialogBar.getInstance().disMissDialog();
                SignInModel signInModel = response.body();
                if (null != signInModel) {
                    if (signInModel.getStatus() == Constants.STATUS_Failure && signInModel.getMessage().equalsIgnoreCase(Constants.NO_USER_EXIT_STRING)) {
                        signUp();
                    } else {
                        AppPreferences.getInstance().setLoginStatus(Constants.VALUE_YES);
                        Cache.userData = signInModel;
                        dashBoard();
                    }
                }

            }

            @Override
            public void onFailure(Call<SignInModel> call, Throwable t) {
                ProgressDialogBar.getInstance().disMissDialog();
            }
        });
    }
// sign in successfully , go to main screen
    private void dashBoard() {
        Intent dashBoardIntent = new Intent(this, DashBoardActivity.class);
        startActivity(dashBoardIntent);
        finish();
    }
// sign up user
    private void signUp() {
        Intent signUpIntent = new Intent(this, SignUp.class);
        startActivityForResult(signUpIntent, Constants.REQUEST_CODE);
    }

}
