package com.mrright.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.mrright.R;
import com.mrright.Util.AppPreferences;

/**
 * Created by deadgame on 07/06/2018.
 */
// dashboard screen, main screen after login
public class DashBoardActivity extends BaseActivity {

    // set layout , overRide from base class
    @Override
    public int getLayoutRes() {
        return R.layout.activity_dashboard;
    }
// start dating view , user click startDating button
    public void startDating(View view) {
        Intent datingIntent = new Intent(this, StartDatingActivity.class);
        startActivity(datingIntent);
    }

    public void contacts(View view) {
    }
// start settings view, user click settings button
    public void settings(View view) {
        Intent datingIntent = new Intent(this, SettingsActivity.class);
        startActivity(datingIntent);
    }
// sign out user.
    public void signOut(View view) {
        AppPreferences.getInstance().signOut();
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
