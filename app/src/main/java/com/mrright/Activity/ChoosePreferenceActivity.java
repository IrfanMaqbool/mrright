package com.mrright.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.BackgroundColorSpan;
import android.text.style.ClickableSpan;

import android.view.View;
import android.widget.TextView;


import com.mrright.R;
import com.mrright.Util.AppPreferences;
import com.mrright.Util.Cache;
import com.mrright.Util.Constants;
import com.mrright.Widgets.BaseTextView;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


/**
 * Created by deadgame on 07/06/2018.
 */
// preference screen of personalInterest && religion
public class ChoosePreferenceActivity extends BaseActivity {


    private BaseTextView clickList;
    private HashMap<Integer, BackgroundColorSpan> selectedPosMap = new HashMap<>();

    private int prePos = 100;
    BaseTextView title, preferenceHeader;

    ArrayList<Integer> preSelectedPos = new ArrayList<>();
    List<String> preSelectedText = new ArrayList<>();
// init class
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preSelectedText = Arrays.asList(getIntent().getStringExtra("preValue").split("\\s*,\\s*"));
        initView();
        setView();
    }
// init views
    private void initView() {
        title = (findViewById(R.id.title));
        preferenceHeader = (findViewById(R.id.preferenceHeader));
        clickList = findViewById(R.id.clickList);
        clickList.setHighlightColor(Color.TRANSPARENT);
        clickList.setLinkTextColor(getResources().getColor(R.color.lightGrey));

    }
// set view
    private void setView() {

        String chooseString = getIntent().getStringExtra(Constants.RELIGION_INTENT);
        if (null != chooseString && chooseString.equals(Constants.RELIGION_INTENT)) {
            setPreSelectedPos(Constants.RELIGIONS_ARRAY);
            setView(Constants.RELIGION, Constants.RELIGIONS_ARRAY, true);// true for Religion
        } else {
            setPreSelectedPos(Constants.PERSONAL_INTEREST_ARRAY);
            setView(Constants.PERSONAL_INTEREST, Constants.PERSONAL_INTEREST_ARRAY, false); // false for Personal Interest
            setHeaderTitle(getResources().getString(R.string.personalInterests));
            preferenceHeader.setText(getResources().getString(R.string.choossHeadingForFive));
        }

    }
// get previous settings position if any
    private void setPreSelectedPos(String[] personalInterestArray) {
        for (int i = 0; i < personalInterestArray.length; i++) {
            for (int j = 0; j < preSelectedText.size(); j++) {
                if (personalInterestArray[i].equalsIgnoreCase(preSelectedText.get(j))) {
                    preSelectedPos.add(i);
                }
            }
        }
    }

//    private void setPreSelectedPos(String[] personalInterestArray, String alreadySelectedPref) {
//        for (int i = 0; i < personalInterestArray.length; i++) {
//            if (personalInterestArray[i].equalsIgnoreCase(alreadySelectedPref)) {
//                preSelectedPos.add(i);
//            }
//        }
//    }

// set view
    private void setView(String text, String[] array, boolean forSpanCalling) {
        SpannableString ss = new SpannableString(text);
        for (int i = 0; i < array.length; i++) {
            int textStartLength = 0;
            for (int j = 0; j < i; j++) {
                textStartLength = textStartLength + array[j].length() + 3;

            }
            if (forSpanCalling) {
                ss.setSpan(new ReligionClickableSpan(i, textStartLength, array[i].length() + textStartLength, ss), textStartLength, array[i].length() + textStartLength, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            } else {
                ss.setSpan(new PersonalInterest(i, textStartLength, array[i].length() + textStartLength, ss), textStartLength, array[i].length() + textStartLength, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }

        clickList.setText(ss);
        clickList.setMovementMethod(LinkMovementMethod.getInstance());
    }

// set religion view
    public class ReligionClickableSpan extends ClickableSpan {

        int pos;
        int textStartLength, textEndLength;
        SpannableString ss;

        public ReligionClickableSpan(int position, int textStartLength, int textEndLength, SpannableString ss) {
            this.pos = position;
            this.textStartLength = textStartLength;
            this.textEndLength = textEndLength;
            this.ss = ss;

            setByDefaultSelect(pos, textStartLength, textEndLength, ss);
        }

        @Override
        public void onClick(View widget) {
            if (null != selectedPosMap.get(pos)) {
                ss.removeSpan((selectedPosMap.get(pos)));
                selectedPosMap.remove(pos);
            } else {
                if (null != selectedPosMap.get(prePos))
                    ss.removeSpan(selectedPosMap.get(prePos));
                selectedPosMap.remove(prePos);
                BackgroundColorSpan backgroundColorSpan = new BackgroundColorSpan(getResources().getColor(R.color.appStyleColor));
                ss.setSpan(backgroundColorSpan, textStartLength, textEndLength, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                selectedPosMap.put(pos, backgroundColorSpan);
            }
            prePos = pos;
            clickList.setText(ss);
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    }
// set previous settings by getting position
    private void setByDefaultSelect(int pos, int textStartLength, int textEndLength, SpannableString ss) {
        if (preSelectedPos.contains(pos)) {
            BackgroundColorSpan backgroundColorSpan = new BackgroundColorSpan(getResources().getColor(R.color.appStyleColor));
            ss.setSpan(backgroundColorSpan, textStartLength, textEndLength, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            selectedPosMap.put(pos, backgroundColorSpan);
            prePos = pos;
        }
    }

// set personalInterest view
    public class PersonalInterest extends ClickableSpan {

        int pos;
        String text;
        int textStartLength, textEndLength;
        SpannableString ss;

        public PersonalInterest(int position, int textStartLength, int textEndLength, SpannableString ss) {
            this.pos = position;
            this.textStartLength = textStartLength;
            this.textEndLength = textEndLength;
            this.ss = ss;
            setByDefaultSelect(pos, textStartLength, textEndLength, ss);
        }

        @Override
        public void onClick(View widget) {
            if (null != selectedPosMap.get(pos)) {
                ss.removeSpan((selectedPosMap.get(pos)));
                selectedPosMap.remove(pos);
            } else {
                if (selectedPosMap.size() < 5) {
                    BackgroundColorSpan backgroundColorSpan = new BackgroundColorSpan(getResources().getColor(R.color.appStyleColor));
                    ss.setSpan(backgroundColorSpan, textStartLength, textEndLength, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    selectedPosMap.put(pos, backgroundColorSpan);
                }
            }
            clickList.setText(ss);
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    }
// save user selection
    public void savePref(View view) {
        if (selectedPosMap.size() > 0) {
            Intent savePrefIntent = new Intent();
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.PREF_KEY, getSelectedValue());
            savePrefIntent.putExtra(Constants.PREF_KEY, bundle);
            setResult(RESULT_OK, savePrefIntent);
            this.finish();
        }
    }

// get selected value
    private ArrayList<Integer> getSelectedValue() {
        ArrayList<Integer> posList = new ArrayList<>();
        Iterator it = selectedPosMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            int index = (int) pair.getKey();
            posList.add(index);

            it.remove();
        }
        return posList;
    }

// set custom header, override from base class
    @Override
    public String getHeaderTitle() {
        return getResources().getString(R.string.religion);
    }
// set layout , overRide from base class
    @Override
    public int getLayoutRes() {
        return R.layout.choose_preference;
    }
}
