package com.mrright.Model.StartDatingModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by deadgame on 08/06/2018.
 */
// StartDatingModel params coming from api
public class StartDatingModel implements Serializable {

    private Long status;
    private String message;
    private ArrayList<Match> matches = null;

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Match> getMatches() {
        return matches;
    }

    public void setMatches(ArrayList<Match> matches) {
        this.matches = matches;
    }

}
