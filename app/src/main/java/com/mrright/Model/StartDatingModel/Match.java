package com.mrright.Model.StartDatingModel;

import com.mrright.Model.CommonModel.Location;
import com.mrright.Model.CommonModel.Preferences;

import java.io.Serializable;
import java.util.List;

/**
 * Created by deadgame on 08/06/2018.
 */
// start dating params, user data coming from api
public class Match implements Serializable {
    private Location location;
    private Preferences preferences;
    private long similarity;
    private List<Object> waiting_contacts = null;
    private long yes_res_count;
    private List<String> personal_interests = null;
    private List<Object> fb_friends = null;
    private List<Object> ln_friends = null;
    private List<Object> maybe_contacts = null;
    private long age;
    private String _id;
    private String dob;
    private String aboutMe;
    private String email;
    private long fbid;
    private String phone;
    private String gender;
    private String Full_name;
    private long __v;
    private String dev_token;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Preferences getPreferences() {
        return preferences;
    }

    public void setPreferences(Preferences preferences) {
        this.preferences = preferences;
    }

    public long getSimilarity() {
        return similarity;
    }

    public void setSimilarity(long similarity) {
        this.similarity = similarity;
    }

    public List<Object> getWaiting_contacts() {
        return waiting_contacts;
    }

    public void setWaiting_contacts(List<Object> waiting_contacts) {
        this.waiting_contacts = waiting_contacts;
    }

    public long getYes_res_count() {
        return yes_res_count;
    }

    public void setYes_res_count(long yes_res_count) {
        this.yes_res_count = yes_res_count;
    }

    public List<String> getPersonal_interests() {
        return personal_interests;
    }

    public void setPersonal_interests(List<String> personal_interests) {
        this.personal_interests = personal_interests;
    }

    public List<Object> getFb_friends() {
        return fb_friends;
    }

    public void setFb_friends(List<Object> fb_friends) {
        this.fb_friends = fb_friends;
    }

    public List<Object> getLn_friends() {
        return ln_friends;
    }

    public void setLn_friends(List<Object> ln_friends) {
        this.ln_friends = ln_friends;
    }

    public List<Object> getMaybe_contacts() {
        return maybe_contacts;
    }

    public void setMaybe_contacts(List<Object> maybe_contacts) {
        this.maybe_contacts = maybe_contacts;
    }

    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getFbid() {
        return fbid;
    }

    public void setFbid(long fbid) {
        this.fbid = fbid;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFull_name() {
        return Full_name;
    }

    public void setFull_name(String full_name) {
        Full_name = full_name;
    }

    public long get__v() {
        return __v;
    }

    public void set__v(long __v) {
        this.__v = __v;
    }

    public String getDev_token() {
        return dev_token;
    }

    public void setDev_token(String dev_token) {
        this.dev_token = dev_token;
    }

}