package com.mrright.Model.CommonModel;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by deadgame on 08/06/2018.
 */
// Preferences data coming from api
public class Preferences implements Serializable {
    private List<String> gender = null;
    private long age_max;
    private long age_min;
    private long dist;
    private String looking_for;

    public List<String> getGender() {
        return gender;
    }

    public void setGender(List<String> gender) {
        this.gender = gender;
    }

    public long getAge_max() {
        return age_max;
    }

    public void setAge_max(long age_max) {
        this.age_max = age_max;
    }

    public long getAge_min() {
        return age_min;
    }

    public void setAge_min(long age_min) {
        this.age_min = age_min;
    }

    public long getDist() {
        return dist;
    }

    public void setDist(long dist) {
        this.dist = dist;
    }

    public String getLooking_for() {
        return looking_for;
    }

    public void setLooking_for(String looking_for) {
        this.looking_for = looking_for;
    }

}