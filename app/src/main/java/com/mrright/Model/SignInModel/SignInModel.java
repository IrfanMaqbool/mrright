package com.mrright.Model.SignInModel;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by deadgame on 06/06/2018.
 */
// SignInModel params coming from api
public class SignInModel implements Serializable {
    private int status;
    private String message;
    private Data data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
